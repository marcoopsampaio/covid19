import numpy as np
from scipy import integrate

N_PEAK_KEY = 'n_peak'
T_PEAK_KEY = 't_peak'
INITIAL_DOUBLING_TIME_KEY = 'initial_doubling_time'
N_1_YEAR_KEY = 'n_1year'


"""
Logistic model
x[0]: square root of the Final cumulative counts
x[1]: square root of t_peak
x[2]: square root of initial lifetime (t0)
"""
def n_logistic(t, x):
    return x[0] ** 2 / (1 + np.exp((x[1] ** 2 - t)/x[2]**2))


def dn_dt_logistic(t, x):
    return n_logistic(t, x) * (1 - n_logistic(t, x) / x[0] ** 2)/ x[2]**2


def get_params_logistic(x):
    return {
        N_PEAK_KEY: int(round(x[0] ** 2 / 2, 0)),
        T_PEAK_KEY: round(x[1] ** 2, 0),
        INITIAL_DOUBLING_TIME_KEY: np.round(x[2] ** 2 / np.log(2)),
        N_1_YEAR_KEY: int(round(n_logistic(365, x), 0))
    }


def logistic_guess(series):
    return [np.sqrt(np.max(series)), np.sqrt(14), np.sqrt(2)]


"""
Double exponential model
x[0]: Cumulative counts at the peak
x[1]: square root of t_peak
x[2]: square root of t_peak/t0
x[3]: square root of the time from start until the first data point
"""


def f_(y, r):
    return integrate.quad(
        lambda x: np.exp(r * (x * np.exp(-x) - 1)) - np.exp(-r), 0, y)[0]


f = np.vectorize(f_)


def n_double_exp(t, x):
    return x[0] ** 2 * f((t - x[3]) / x[1] ** 2, x[2] ** 2) / f(1, x[2] ** 2)


#n_double_exp = np.vectorize(n_double_exp)


def dn_dt_double_exp(t, x):
    tpeak = x[1] ** 2
    y = (t - x[3]) / tpeak
    r = x[2] ** 2
    return x[0] ** 2 / tpeak * (np.exp(r * (y * np.exp(-y) - 1)) - np.exp(-r)) / f(1, r)


#dn_dt_double_exp = np.vectorize(dn_dt_double_exp)


def get_params_double_exp(x):
    def safe_to_int(y):
        if y * 0 == 0:
            return int(y)
        return y
    t_at_peak = round(x[1] ** 2 + x[3], 0)
    return {
        N_PEAK_KEY: safe_to_int(np.round(n_double_exp(t_at_peak, x), 0)),
        #N_PEAK_KEY: safe_to_int(np.round(x[0] ** 2, 0)),
        T_PEAK_KEY: t_at_peak,
        INITIAL_DOUBLING_TIME_KEY: np.round((x[1] / x[2]) ** 2 / np.log(2), 2),
        N_1_YEAR_KEY: safe_to_int(np.round(n_double_exp(365, x), 0))
    }


def double_exp_guess(series):
    return [np.sqrt(np.max(series)), np.sqrt(20), np.sqrt(15), np.sqrt(5)]
