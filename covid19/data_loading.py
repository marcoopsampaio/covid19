import pandas as pd
import os
from covid19.commons import PATH_DATA

CSV_CONFIRMED = "time_series_covid_19_confirmed.csv"
CSV_DEATHS = "time_series_covid_19_deaths.csv"
CSV_RECOVERED = "time_series_covid_19_recovered.csv"


def clean_df(df):
    df = df[
        [df.columns[1]] + list(df.columns[4:])
    ].groupby(df.columns[1]).agg(sum).transpose()
    df.index = [pd.Timestamp(value) for value in df.index.values]
    df["date"] = df.index.values
    return df


def load_all_dfs(path: str = PATH_DATA):
    df_confirmed = clean_df(pd.read_csv(os.path.join(path, CSV_CONFIRMED)))
    df_deaths = clean_df(pd.read_csv(os.path.join(path, CSV_DEATHS)))
    df_recovered = clean_df(pd.read_csv(os.path.join(path, CSV_RECOVERED)))
    return df_confirmed, df_deaths, df_recovered
