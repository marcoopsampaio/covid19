import pickle

import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
from dash.dependencies import Input, Output

from covid19.model_functions import *
from covid19.optimization import *
from covid19.plotting import *

global all_results_deaths
global dict_countries_deaths
global all_results_confirmed
global dict_countries_confirmed

# Set up the app
app = dash.Dash(__name__)

server = app.server

DEFAULT_COUNTRIES = ["China", "Portugal", "Italy"]

all_results_deaths = pickle.load(open("../notebooks/all_results_deaths.pkl", "rb"))
all_results_confirmed = pickle.load(
    open("../notebooks/all_results_confirmed.pkl", "rb")
)

global colors_list
global colors_list_faint

colors_list = [
    '#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2',
    '#7f7f7f', '#bcbd22', '#17becf'
]

colors_list_faint = [
    '#96c9ed', '#ffc08a', '#8bdf8b', '#ea8f8f', '#c5addb', '#c8a098', '#efb3dd',
    '#bdbdbd', '#e8e87d', '#71e3ef'
]

URL_BLOG_POST = 'https://medium.com/@marcoopsampaio/covid-19-a-surprisingly-effective-data-driven-model-1a3bb0361d7a?sk=2460a64b45ff4b63cf666dc9274eee31'
GIT_LAB_REPO = 'https://gitlab.com/marcoopsampaio/covid19'


def create_dict_list_of_countries(results):
    dictlist = []
    for country in results.keys():
        dictlist.append({'value': country, 'label': country})
    return dictlist


dict_countries_deaths = create_dict_list_of_countries(all_results_deaths)
dict_countries_confirmed = create_dict_list_of_countries(all_results_confirmed)


app.layout = html.Div([
    html.Div([
        html.H1('Covid-19: A surprisingly effective data driven model'),
        html.H4([
            html.A('by Marco O. P. Sampaio',
                   href='https://www.linkedin.com/in/marcoopsampaio/'
                   )
        ], style={'color': 'gray'}),
    ], style={'width': '100%'}
    ),
    html.Div([
        html.H6([
            'IMPORTANT INFORMATION: This data and forecasts are for illustration '
            'purposes only and do not replace forecasts from legitimate authorities. '
            'Do not use them for any decision making!'
        ]),
        html.P([
            'The dashboards on this page contain data and model fits for '
            'death counts (section 1) and confirmed infection counts (section 2)  '
            'from Covid-19. The source data is from the Kaggle dataset described ',
            html.A("here",
                   href='https://www.kaggle.com/sudalairajkumar/novel-corona-virus-2019-dataset',
                   target="_blank"),
            '. The model used for forecasting is the double exponential model '
            'I have presented in ',
            html.A("this medium blog post",
                   href=URL_BLOG_POST,
                   target="_blank"),
            '. The code can be found in this ',
            html.A("gitlab repo",
                   href=URL_BLOG_POST,
                   target="_blank"),
            '.',
        ]),
    ], style={'width': '100%', 'display': 'inline-block'}),
    html.Div([
        html.Div([
            html.H2('1. Deaths'),
            html.P([
                'Here we present forecasts for death counts. The graphs are '
                'interactive. Move your mouse over the plots to display the available '
                'toolbox or change the countries using the dropdown on the right.',
            ]),
        ], style={'width': '100%'}
        ),
        html.Div([
            html.Div([
                html.H6('Choose Countries:'),
                dcc.Dropdown(
                    id='country-dropdown',
                    options=dict_countries_deaths,
                    multi=True,
                    value=DEFAULT_COUNTRIES
                ),
            ], style={'width': '100%', 'display': 'inline-block'}
            ),
            html.Div([
                html.H6('Parameters:'),
                html.Table(id='forecast-table'),
                html.P(''),
            ], style={'width': '100%', 'display': 'inline-block'}),
        ], style={'width': '27%', 'float': 'left', 'display': 'inline-block'}
        ),
        html.Div([
            dcc.Graph(id='total-deaths-graph'),
            html.P('')
        ], style={'width': '36%', 'display': 'inline-block'}),
        html.Div([
            dcc.Graph(id='deaths-per-day-graph'),
            html.P('')
        ], style={'width': '36%', 'display': 'inline-block'}),
    ], style={'width': '100%'}),
    html.Div([
        html.H2('2. Confirmed infections'),
    ], style={'width': '100%', 'float': 'left',}
    ),
    html.Div([
        html.P([
            'Though the public data for confirmed infections is often noisier'
            '(it\'s quality largely depends on many human factors - see ',
            html.A("discussion in medium blog post",
                   href=URL_BLOG_POST,
                   target="_blank")
            ,
            '), here we still present results for confirmed infection '
            'counts. We can clearly see, in some cases, that the counting '
            'procedure (which largely depends on the amount of testing and reporting) '
            'suffers from sudden jumps over time -- see e.g. the data for China where '
            'a sudden jump appears in the data at some point in February.'
        ]),
    ], style={'width': '100%', 'display': 'inline-block'}
    ),
    html.Div([
        html.Div([
            html.H6('Choose Countries:'),
            dcc.Dropdown(
                id='country-dropdown-confirmed',
                options=dict_countries_confirmed,
                multi=True,
                value=DEFAULT_COUNTRIES
            ),
        ], style={'width': '100%', 'display': 'inline-block'}
        ),
        html.Div([
            html.H6('Parameters:'),
            html.Table(id='forecast-table-confirmed'),
            html.P(''),
        ], style={'width': '100%', 'display': 'inline-block'}),
    ],  style={'width': '28%', 'float': 'left', 'display': 'inline-block'}),
    html.Div([
        dcc.Graph(id='total-confirmed-graph'),
        html.P('')
    ], style={'width': '36%', 'display': 'inline-block'}),
    html.Div([
        dcc.Graph(id='confirmed-per-day-graph'),
        html.P('')
    ], style={'width': '36%', 'display': 'inline-block'}),
    html.Div([
        html.P([
            'Disclaimer: If you happen to bump into a  fit that seems to not even '
            'remotely attempt to adjust to the data points, it probably just means that '
            'the optimizer that runs daily on the updated data crashed. This is '
            'likely to solve itself in the next daily update. ',
            'Please also read the disclaimers in the ',
            html.A("medium blog post",
                   href=URL_BLOG_POST,
                   target="_blank")
            ,
            '.',
        ]),
    ], style={'width': '100%', 'float': 'left'}),
])


@app.callback(Output('total-deaths-graph', 'figure'), [Input('country-dropdown', 'value')])
def update_graph(selected_dropdown_value):

    results_filtered = results_filtered_func_deaths(selected_dropdown_value)

    data, xmin, xmax = timeline_countries_filtered_by_keys(
        results_filtered, PREDICTED_CUMULATIVE_PLUS_KEY, CUMULATIVE_KEY)

    # Edit the layout
    layout = dict(title='Total Deaths',
                  xaxis=dict(range=[xmin, xmax]),
                  yaxis=dict(title='counts'),
                  )
    figure = dict(data=data, layout=layout)
    return figure


@app.callback(Output('deaths-per-day-graph', 'figure'), [Input('country-dropdown', 'value')])
def update_graph(selected_dropdown_value):

    results_filtered = results_filtered_func_deaths(selected_dropdown_value)

    data, xmin, xmax = timeline_countries_filtered_by_keys(
        results_filtered, PREDICTED_DENSITY_PLUS_KEY, DENSITY_KEY)

    # Edit the layout
    layout = dict(title='Deaths per day',
                  xaxis=dict(range=[xmin, xmax]),
                  yaxis=dict(title='counts')
                  )
    figure = dict(data=data, layout=layout)
    return figure

@app.callback(Output('total-confirmed-graph', 'figure'),
              [Input('country-dropdown-confirmed', 'value')])
def update_graph(selected_dropdown_value):

    results_filtered = results_filtered_func_confirmed(selected_dropdown_value)

    data, xmin, xmax = timeline_countries_filtered_by_keys(
        results_filtered, PREDICTED_CUMULATIVE_PLUS_KEY, CUMULATIVE_KEY
    )

    # Edit the layout
    layout = dict(title='Total Confirmed infections',
                  xaxis=dict(range=[xmin, xmax]),
                  yaxis=dict(title='counts'),
                  )
    figure = dict(data=data, layout=layout)
    return figure


@app.callback(Output('confirmed-per-day-graph', 'figure'),
              [Input('country-dropdown-confirmed', 'value')])
def update_graph(selected_dropdown_value):

    results_filtered = results_filtered_func_confirmed(selected_dropdown_value)

    data, xmin, xmax = timeline_countries_filtered_by_keys(
        results_filtered, PREDICTED_DENSITY_PLUS_KEY, DENSITY_KEY
    )

    # Edit the layout
    layout = dict(title='Confirmed infections per day',
                  xaxis=dict(range=[xmin, xmax]),
                  yaxis=dict(title='counts'),
                  )
    figure = dict(data=data, layout=layout)
    return figure


def results_filtered_func_deaths(selected_dropdown_value):
    return {
        key: value
        for key, value in all_results_deaths.items()
        if key in selected_dropdown_value
    }


def results_filtered_func_confirmed(selected_dropdown_value):
    return {
        key: value
        for key, value in all_results_confirmed.items()
        if key in selected_dropdown_value
    }


def timeline_countries_filtered_by_keys(results_filtered, key_prediction, key):
    # Make a timeline
    trace_list = []

    xmin = None
    xmax = pd.Timestamp(0)

    i = 0
    for country, results in results_filtered.items():
        color_index = (i % (len(colors_list)))
        color = colors_list[color_index]
        color_faint = colors_list_faint[color_index]
        i += 1
        x_vals2 = results[key_prediction].index
        trace1 = go.Scatter(
            x=x_vals2,
            y=results[key_prediction],
            mode='lines',
            name="Prediction",
            showlegend=False,
            line=dict(color=color_faint, width=2)
        )
        trace_list.append(trace1)
        x_vals = results[key].index
        trace2 = go.Scatter(
            x=x_vals,
            y=results[key],
            mode='markers',
            name=country,

            marker=dict(
                color='rgba(0, 0, 0, 0.)',
                line=dict(
                    color=color,
                    width=1
                )
            )
        )
        trace_list.append(trace2)
        xmax_this = x_vals2[min(len(x_vals) + 30, max(2 * len(x_vals),
                                                      len(x_vals) + 60))]
        if xmin is None:
            xmin = x_vals[0]
        xmin = min(x_vals[0], xmin)
        xmax = max(xmax_this, xmax)

    return trace_list, xmin, xmax


# for the table
@app.callback(Output('forecast-table', 'children'),
              [Input('country-dropdown', 'value')])
def generate_table(selected_dropdown_value):
    results_filtered = results_filtered_func_deaths(selected_dropdown_value)
    return get_html_table(results_filtered)


# for the table
@app.callback(Output('forecast-table-confirmed', 'children'),
              [Input('country-dropdown-confirmed', 'value')])
def generate_table(selected_dropdown_value):
    results_filtered = results_filtered_func_confirmed(selected_dropdown_value)
    return get_html_table(results_filtered)


def get_html_table(results_filtered):
    use_keys = [T_PEAK_KEY, N_PEAK_KEY, N_1_YEAR_KEY]
    header = [html.Th("Country")]
    for col in use_keys:
            header.append(html.Th(col))

    header = [html.Th("Country")]
    for col in use_keys:
        if col == INITIAL_DOUBLING_TIME_KEY:
            header.append(html.Th("Initial doubling #days"))
        elif col == N_PEAK_KEY:
            header.append(html.Th("Total @peak"))
        elif col == T_PEAK_KEY:
            header.append(html.Th("Peak date"))
        elif col == N_1_YEAR_KEY:
            header.append(html.Th("1 Year Total"))
        else:
            header.append(html.Th(col))
    if len(results_filtered) != 0:
        rows = []
        for country, result in results_filtered.items():
            row_cols = [html.Td(country)]
            for key in use_keys:
                value = result[PREDICTED_INTERPRETABLE_PARAMS_KEY][key]
                if result[PREDICTED_INTERPRETABLE_PARAMS_KEY][N_1_YEAR_KEY] > \
                        2 * LARGENESS_FACTOR * np.max(result[CUMULATIVE_KEY]):
                    value = "NA"
                else:
                    if key == T_PEAK_KEY:
                        value = result[CUMULATIVE_KEY].index[0] + \
                                result[PREDICTED_INTERPRETABLE_PARAMS_KEY][key] * \
                                pd.Timedelta('1d')
                        value = value.date()
                    if key == N_PEAK_KEY or key == N_1_YEAR_KEY:
                        if 10000 < value <= 10000000:
                            value = str(np.round(value / 1000.)) + "K"
                        elif 10000000 < value <= 10000000000:
                            value = str(np.round(value / 1000000.)) + "M"
                        elif value > 10000000000:
                            value = str(np.round(value / 1000000000.)) + "B"
                    if key == INITIAL_DOUBLING_TIME_KEY:
                        value = str(value)
                row_cols.append(html.Td(value))

            rows.append(html.Tr(row_cols))
        return [html.Tr(header)] + rows
    return [html.Tr(header)]


if __name__ == '__main__':
    app.run_server(debug=True)