import numpy as np
import pandas as pd

DEFAULT_N_WINDOW = 7


def doubling_time_df(df, n_window=DEFAULT_N_WINDOW):
    countries = [country for country in df.columns if country != "date"]
    df_out = df.iloc[n_window:]
    for country in countries:
        df_out[country] = doubling_time(
            df[country].values, n_window=n_window
        )
    return df_out


def doubling_time(series, n_window=DEFAULT_N_WINDOW):
    return n_window * np.log(2)/np.log(series[n_window:]/series[:-n_window])


def differentiate(series, n_window=DEFAULT_N_WINDOW):
    return (series.values[n_window:] - series.values[:-n_window]) / n_window
