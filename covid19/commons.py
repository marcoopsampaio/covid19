import os

PATH_ROOT_REPO =os.path.abspath(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), "..")
)
PATH_DATA = os.path.join(PATH_ROOT_REPO, "data")
