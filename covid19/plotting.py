import plotly.graph_objects as go
import pandas as pd
import numpy as np
from covid19.optimization import get_results_series
from covid19.optimization import (
    CUMULATIVE_KEY,
    DENSITY_KEY,
    PREDICTED_CUMULATIVE_PLUS_KEY,
    PREDICTED_DENSITY_PLUS_KEY,
    PREDICTED_INTERPRETABLE_PARAMS_KEY
)
from covid19.model_functions import T_PEAK_KEY, N_1_YEAR_KEY, N_PEAK_KEY
from IPython.display import display
from plotly.subplots import make_subplots


def plot_several_countries(
        df, countries, title, y_axis_type=None, yaxis_title="counts"
):
    fig = go.Figure()
    for country in countries:
        fig.add_trace(
            go.Scatter(x=df['date'], y=df[country], mode='markers', name=country)
        )
    fig.update_layout(
        title=title,
        xaxis_title="date",
        yaxis_title=yaxis_title,
        yaxis_type=y_axis_type
    )

    fig.show()


def plot_results_fit(title: str, results_dict: dict):
    color_points_cumulative = "#636efa"
    color_lines_cumulative = "#ff6692"
    color_points_density = color_points_cumulative
    color_lines_density = color_lines_cumulative
    results_dict[PREDICTED_INTERPRETABLE_PARAMS_KEY][T_PEAK_KEY] = \
        results_dict[CUMULATIVE_KEY].index[0] + \
        results_dict[PREDICTED_INTERPRETABLE_PARAMS_KEY][T_PEAK_KEY] * \
        pd.Timedelta('1d')
    display(
        pd.DataFrame({
            key: [value]
            for key, value in results_dict[PREDICTED_INTERPRETABLE_PARAMS_KEY].items()
        }, index=["Parameters"])
    )

    N_DAYS = 10
    display(
        pd.DataFrame(
            data={
                f"Predicted total counts": np.around(
                    results_dict[
                        PREDICTED_CUMULATIVE_PLUS_KEY
                    ][len(results_dict[CUMULATIVE_KEY]):].values[:N_DAYS], 0
                )
            },
            index=results_dict[
                      PREDICTED_CUMULATIVE_PLUS_KEY
                  ][len(results_dict[CUMULATIVE_KEY]):].index[:N_DAYS]
        ).transpose()
    )

    x_vals = results_dict[CUMULATIVE_KEY].index
    x_vals2 = results_dict[PREDICTED_CUMULATIVE_PLUS_KEY].index
    xmin = x_vals[0]
    xmax = x_vals2[min(len(x_vals) + 30, max(2 * len(x_vals), len(x_vals) + 60))]

    fig = make_subplots(
        rows=1,
        cols=2,
        subplot_titles=("Total Cases", "Cases per day")
    )

    fig.add_trace(
        go.Scatter(
            x=x_vals2,
            y=results_dict[PREDICTED_CUMULATIVE_PLUS_KEY],
            mode='lines',
            name="predicted",
            line=dict(color=color_lines_cumulative)
        ),
        row=1, col=1
    )
    fig.add_trace(
        go.Scatter(
            x=x_vals, y=results_dict[CUMULATIVE_KEY], mode='markers',
            name="actual",
            line=dict(color=color_points_cumulative)
        ),
        row=1, col=1
    )

    fig.add_trace(
        go.Scatter(
            x=x_vals2,
            y=results_dict[PREDICTED_DENSITY_PLUS_KEY],
            mode='lines',
            showlegend=False,
            line=dict(color=color_lines_density)
        ),
        row=1, col=2
    )
    fig.add_trace(
        go.Scatter(
            x=x_vals, y=results_dict[DENSITY_KEY], mode='markers',
            showlegend=False,
            line=dict(color=color_points_density)
        ),
        row=1, col=2
    )
    fig.update_layout(title_text=title,
                      xaxis1={"range": [xmin, xmax]},
                      xaxis2={"range": [xmin, xmax]},
                      xaxis3={"range": [xmin, xmax]},
                      xaxis4={"range": [xmin, xmax]}
                      )
    fig.show()


def get_results_and_report(
        title: str,
        series: pd.Series, model_func, initial_guess_func, density_func, get_params_func
):
    results = get_results_series(
        series, model_func, initial_guess_func, density_func, get_params_func
    )
    plot_results_fit(title, results)
    return results


def plot_iterate_predictions(
        title,
        series: pd.Series, model_func, initial_guess_func, density_func,
        get_params_func,
        n_points_min=7
):
    all_results = []
    series_with_values = series[series > 0]
    x_vals = series_with_values.index[n_points_min: len(series_with_values)]
    i_vals = [i for i in range(n_points_min, len(series_with_values))]
    for i in i_vals:
        all_results.append(
            get_results_series(
                series_with_values[:i + 1], model_func, initial_guess_func,
                density_func,
                get_params_func
            )
        )
    to_plot = {
        T_PEAK_KEY: [],
        N_1_YEAR_KEY: [],
        N_PEAK_KEY: []
    }

    for result in all_results:
        for key in to_plot.keys():
            to_plot[key].append(result[PREDICTED_INTERPRETABLE_PARAMS_KEY][key])

    fig = make_subplots(
        rows=1,
        cols=3,
        subplot_titles=("Predicted Peak Date", "Predicted Total @ 1 year",
                        "Predicted Total @ peak")
    )

    fig.add_trace(
        go.Scatter(
            x=x_vals,
            y=[series_with_values.index[0] + val * pd.Timedelta('1d')
               for val in to_plot[T_PEAK_KEY]],
            mode='markers',
            showlegend=False,
        ),
        row=1, col=1
    )

    fig.add_trace(
        go.Scatter(
            x=x_vals,
            y=to_plot[N_1_YEAR_KEY],
            mode='markers',
            showlegend=False,
        ),
        row=1, col=2
    )

    fig.add_trace(
        go.Scatter(
            x=x_vals,
            y=to_plot[N_PEAK_KEY],
            mode='markers',
            showlegend=False,
        ),
        row=1, col=3
    )

    def get_max_plot(key_):
        x = 3 * np.median(to_plot[key_])
        return 1.05 * max([value for value in to_plot[key_] if value <= x])

    ymax1 = series_with_values.index[0] + pd.Timedelta('1d') * get_max_plot(T_PEAK_KEY)
    ymax2 = get_max_plot(N_1_YEAR_KEY)
    ymax3 = get_max_plot(N_PEAK_KEY)

    fig.update_layout(
        title=title,
        yaxis1={'range': [series_with_values.index[0], ymax1]},
        yaxis2={'range': [0, ymax2]},
        yaxis3={'range': [0, ymax3]}
    )
    fig.show()
