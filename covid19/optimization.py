from scipy.optimize import least_squares, curve_fit
import pandas as pd
import numpy as np

CUMULATIVE_KEY = 'cumulative'
DENSITY_KEY = 'density'
# PREDICTED_CUMULATIVE_KEY = 'predicted_cumulative'
# PREDICTED_DENSITY_KEY = 'predicted_density'
PREDICTED_CUMULATIVE_PLUS_KEY = 'predicted_cumulative_plus'
PREDICTED_DENSITY_PLUS_KEY = 'predicted_density_plus'
PREDICTED_PARAMS_KEY = 'predicted_params'
PREDICTED_INTERPRETABLE_PARAMS_KEY = 'predicted_interpretable_params'

LARGENESS_FACTOR = 5
MIN_COUNTS = 100
MIN_N_DAYS = 14


def residuals(model_func, ny_values, x):
    return [ny - model_func(t, x)
            for ny, t in zip(ny_values, range(len(ny_values)))]


def fit_to_model(series, model_func, initial_guess):
    return least_squares(
        lambda x: residuals(model_func, series, x), initial_guess,method='lm'
    )['x']


# def fit_to_model(series, model_func, initial_guess):
#     def m_func(t, *x):
#         return model_func(t, x)
#     x_vals = [i for i in range(len(series))]
#     #tol = 1e-4
#     try:
#         # If possible try to use curve_fit so that we get uncertainties
#         results = curve_fit(m_func, x_vals, series, p0=initial_guess, method='lm')
#     except:
#         results = (
#             least_squares(
#                 lambda x: residuals(model_func, series, x), initial_guess, method='lm'
#             )['x'],
#             None
#         )
#
#     return results[0], results[1]


def get_params_distribution(
        series: pd.Series, model_func, initial_guess_func, density_func,
):
    reference_params = get_params_series(
        series=series,
        model_func=model_func,
        initial_guess_func=initial_guess_func,
    )
    dn_dt_predict = np.array([density_func(val, reference_params) for val in series])
    resids_sq = np.abs(dn_dt_predict[1:] - series.diff()[1:])
    std_resids = np.std(resids_sq)

    # Generate several samples assuming gaussian noise
    x_vals = [i for i in range(len(series))]
    params_distrib = []
    for i in range(35):
        series_iter = np.round(np.random.normal(loc=0, scale=std_resids, size=len(
            x_vals)) + dn_dt_predict)
        series_iter[series_iter < 0] = 0
        series_iter = pd.Series(series_iter).cumsum()
        params_iter = get_params_series(
            series=series_iter,
            model_func=model_func,
            initial_guess_func=initial_guess_func,
        )
        params_distrib.append(params_iter)

    return params_distrib


def get_params_series(
        series: pd.Series, model_func, initial_guess_func, cumulative=None
):
    if cumulative is None:
        cumulative = series[series > 0]
    predicted_params = fit_to_model(
        series=cumulative,
        model_func=model_func,
        initial_guess=initial_guess_func(cumulative)
    )
    return predicted_params


def get_results_plus_from_params(
        predicted_params, series: pd.Series, model_func, density_func
):
    cumulative = series[series > 0]

    series2_len = len(cumulative) + 180
    index2 = [
        cumulative.index.values[0] + i * + pd.Timedelta('1d')
        for i in range(series2_len)
    ]

    predicted_cumulative_plus = np.round(
        pd.Series(
            data=[model_func(i, predicted_params) for i in range(series2_len)],
            index=index2
        ),
        2
    )

    predicted_density_plus = np.round(
        pd.Series(
            data=[density_func(i, predicted_params) for i in range(series2_len)],
            index=index2
        ), 2
    )
    mask = predicted_cumulative_plus > LARGENESS_FACTOR * np.max(cumulative)
    predicted_cumulative_plus[mask] = np.nan
    predicted_density_plus[mask] = np.nan

    return {
        PREDICTED_CUMULATIVE_PLUS_KEY: predicted_cumulative_plus,
        PREDICTED_DENSITY_PLUS_KEY: predicted_density_plus,
    }


def get_results_series(
        series: pd.Series, model_func, initial_guess_func, density_func, get_params_func
):
    cumulative = series[series > 0]
    assert len(cumulative) > MIN_N_DAYS and cumulative[-1] > MIN_COUNTS, \
        "Not enough data to attempt predictions for this country"
    density = cumulative.diff()
    predicted_params = get_params_series(
        series=series, model_func=model_func, initial_guess_func=initial_guess_func,
        cumulative=cumulative
    )
    predicted_plus = get_results_plus_from_params(
        predicted_params, cumulative, model_func, density_func
    )
    predicted_cumulative_plus = predicted_plus[PREDICTED_CUMULATIVE_PLUS_KEY]
    predicted_density_plus = predicted_plus[PREDICTED_DENSITY_PLUS_KEY]

    predicted_interpretable_params = get_params_func(predicted_params)

    return {
        CUMULATIVE_KEY: cumulative,
        DENSITY_KEY: density,
        PREDICTED_CUMULATIVE_PLUS_KEY: predicted_cumulative_plus,
        PREDICTED_DENSITY_PLUS_KEY: predicted_density_plus,
        PREDICTED_PARAMS_KEY: predicted_params,
        PREDICTED_INTERPRETABLE_PARAMS_KEY: predicted_interpretable_params
    }


def get_results(
        df: pd.DataFrame, model_func, initial_guess_func, density_func, get_params_func
):
    output_dict = dict()
    for country in df.columns:
        if country == 'date':
            continue
        try:
            results = get_results_series(
                df[country], model_func, initial_guess_func, density_func, get_params_func
            )
            output_dict[country] = results
        except:
            print(f"Could not compute fit results for country {country}")

    return output_dict



